#include <iostream>
#include <string>

#define __STRUCT_PATH(x) #x
#define _STRUCT_PATH(x) __STRUCT_PATH(x)

#ifndef STRUCT_PATH
#define STRUCT_PATH _STRUCT_PATH(avl_tree.h)
#endif

#ifndef STRUCT
#define STRUCT AVLTree
#endif

#include STRUCT_PATH
#include "tree_visualizer.h"

auto values_from_args(int argc, char* argv[]) {
    auto values = std::vector<int>{};
    for (auto i = 1u; i < argc; ++i) {
        std::cout << "inserting " << argv[i] << '\n';
        values.push_back(std::stoi(argv[i]));
    }
    return values;
}

int main(int argc, char* argv[]) {
    using namespace structures;
    using namespace structures::visualizer;

    std::cout << "Initializing visualizer...\n";

    const auto values =
        argc > 1 ?
            values_from_args(argc, argv)
        :
            std::vector<int>{
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            };

    visualize_tree<STRUCT<int>>(values);
}
