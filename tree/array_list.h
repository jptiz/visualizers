#ifndef STRUCTURES_ARRAY_LIST_H
#define STRUCTURES_ARRAY_LIST_H

#include <cstdint>

// non-end
#include <stdexcept>
//

namespace structures {

template<typename T>
class ArrayList {
public:
    ArrayList() = default;

    ArrayList(std::size_t max_size):
        contents{new T[max_size]},
        max_size_{max_size},
        size_{0u}
    {}

    void clear() {
        size_ = 0u;
    }

    void push_back(T data) {
        if (size_ >= max_size()) {
            throw std::out_of_range{"List limit reached."};
        }
        contents[size_] = data;
        ++size_;
    }

    void push_front(T data) {
        if (size_ >= max_size()) {
            throw std::out_of_range{"List limit reached."};
        }
        for (auto i = size_; i > 0; --i) {
            contents[i] = contents[i-1];
        }
        contents[0] = data;
        ++size_;
    }

    void insert(std::size_t index, T data) {
        if (index >= size()) {
            throw std::out_of_range{"Index out of list bounds."};
        }
        for (auto i = size_; i > index; --i) {
            contents[i] = contents[i-1];
        }
        contents[index] = data;
        ++size_;
    }

    void insert_sorted(T data) {
        if (size() >= max_size()) {
            throw std::out_of_range{"List limit reached."};
        }
        for (auto i = 0u; i < size(); ++i) {
            if (contents[i] > data) {
                insert(i, data);
                return;
            }
        }
        contents[size()] = data;
        ++size_;
    }

    T pop(std::size_t index) {
        if (index >= size()) {
            throw std::out_of_range{"Index out of list bounds."};
        }
        auto old = contents[index];
        for (auto i = index; i < size_; ++i) {
            contents[i] = contents[i+1];
        }
        --size_;
        return old;
    }

    T pop_back() {
        if (size_ == 0u) {
            throw std::out_of_range{"Empty list: no elements to pop."};
        }
        return contents[--size_];
    }

    T pop_front() {
        if (size_ == 0u) {
            throw std::out_of_range{"Empty list: no elements to pop."};
        }
        auto old = contents[0];
        for (auto i = 0u; i < size_-1; ++i) {
            contents[i] = contents[i+1];
        }
        --size_;
        return old;
    }

    void remove(T data) {
        for (auto i = 0u; i < size(); ++i) {
            if (contents[i] == data) {
                pop(i);
            }
        }
    }

    bool full() const {
        return size_ == max_size_;
    }

    bool empty() const {
        return size_ == 0;
    }

    bool contains(const T& data) const {
        for (auto i = 0u; i < size(); ++i) {
            if (contents[i] == data) return true;
        }
        return false;
    }

    std::size_t find(const T& data) {
        auto i = 0u;
        for (; i < size(); ++i) {
            if (data == contents[i]) {
                return i;
            }
        }
        return i;
    }

    std::size_t size() const {
        return size_;
    }

    std::size_t max_size() const {
        return max_size_;
    }

    T& at(std::size_t index) {
        if (index >= size()) {
            throw std::out_of_range{"Index out of list bounds."};
        }
        return contents[index];
    }

    T& operator[](unsigned index) {
        return contents[index];
    }

    const T& at(std::size_t index) const {
        if (index >= size()) {
            throw std::out_of_range{"Index out of list bounds."};
        }
        return contents[index];
    }

    const T& operator[](unsigned index) const {
        return contents[index];
    }

private:
    T* contents{new T[DEFAULT_SIZE]};
    std::size_t size_{0u};
    std::size_t max_size_{DEFAULT_SIZE};

    const static auto DEFAULT_SIZE = 10u;
};

}

#endif
