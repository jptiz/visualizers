#ifndef STRUCTURES_AVL_TREE_H
#define STRUCTURES_AVL_TREE_H

#include "array_list.h"

#include <iostream>
#include <cmath>
#include <utility>

namespace structures {

template <typename T>
class AVLTree {
public:
    AVLTree() = default;

    void insert(const T& data) {
        if (not root) {
            root = new Node{data};
        } else {
            root->insert(data);
        }
        ++size_;
    }

    void clear() {
        if (root) {
            root->clear();
        }
        delete root;
        root = nullptr;
        size_ = 0u;
    }

    void remove(const T& data) {
        if (root and root->remove(data)) {
            --size_;
        }
    }

    bool contains(const T& data) const {
        if (root) {
            return root->contains(data);
        }
        return false;
    }

    bool empty() const {
        return size_ == 0u;
    }

    std::size_t size() const {
        return size_;
    }

    ArrayList<T> pre_order() const {
        auto ordered{ArrayList<T>{size_}};
        if (root) {
            root->pre_order(ordered);
        }
        return ordered;
    }

    ArrayList<T> in_order() const {
        auto ordered{ArrayList<T>{size_}};
        if (root) {
            root->in_order(ordered);
        }
        return ordered;
    }

    ArrayList<T> post_order() const {
        auto ordered{ArrayList<T>{size_}};
        if (root) {
            root->post_order(ordered);
        }
        return ordered;
    }

private:
    struct Node {
        Node(const T& data):
            data{data}
        {}

        T data;
        std::size_t height_{0};
        Node* left{nullptr};
        Node* right{nullptr};

        void insert(const T& data_) {
            if (data_ <= data) {
                if (left) {
                    left->insert(data_);
                    left = balance(data_, left);
                } else {
                    left = new Node{data_};
                }
            } else {
                if (right) {
                    right->insert(data_);
                    right = balance(data_, right);
                } else {
                    right = new Node{data_};
                }
            }
            height_ = std::max(height(left), height(right)) + 1;
        }

        void clear() {
            if (left) {
                left->clear();
            }
            if (right) {
                right->clear();
            }
            delete left;
            delete right;
        }

        bool remove(const T& data_) {
            return false;
        }

        bool contains(const T& data_) const {
            if (data_ < data and left) {
                return left->contains(data_);
            }
            if (data_ > data and right) {
                return right->contains(data_);
            }
            return data_ == data;
        }

        void pre_order(ArrayList<T>& v) const {
            v.push_back(data);
            if (left) {
                left->post_order(v);
            }
            if (right) {
                right->post_order(v);
            }
        }

        void in_order(ArrayList<T>& v) const {
            if (left) {
                left->post_order(v);
            }
            v.push_back(data);
            if (right) {
                right->post_order(v);
            }
        }

        void post_order(ArrayList<T>& v) const {
            if (left) {
                left->post_order(v);
            }
            if (right) {
                right->post_order(v);
            }
            v.push_back(data);
        }

        int height(Node* node) {
            return node ? node->height_ : -1;
        }
    private:
        auto balance() {
            return height(left) - height(right);
        }

        auto balance(const T& inserted_data, Node*& leaf) {
            if (std::abs(leaf->balance()) > 1) {
                std::cout << "tree is unbalanced at leaf " << leaf->data << "\n";
                if (inserted_data < leaf->data) {
                    auto old_leaf = leaf;
                    leaf = simple_rotation_left();
                    if (leaf) {
                        std::cout << '\t' << old_leaf->data << " turned into " << leaf->data << std::endl;
                    } else {
                        std::cout << '\t' << old_leaf->data << " turned into null" << std::endl;
                    }
                } else {
                    auto old_leaf = leaf;
                    leaf = double_rotation_left();
                    if (leaf) {
                        std::cout << '\t' << old_leaf->data << " turned into " << leaf->data << std::endl;
                    } else {
                        std::cout << '\t' << old_leaf->data << " turned into null" << std::endl;
                    }
                }
            }
            return leaf;
        }

        Node* simple_rotation_left() {
            std::cout << "\tsimple left rotation at node " << data << '\n';
            auto k1 = left;
            if (k1) {
                std::cout << "\t\t(switching " << k1->right->data << " with " << left->data << ")\n";
                left = k1->right;
                k1->right = this;
            } else {
                std::cout << "\t\t(nothing happening feijoada)\n";
            }

            height_ = std::max(height(left), height(right)) + 1;
            if (k1) {
                k1->height_ = std::max(height(k1->right), height(this)) + 1;
            }

            return k1;
        }

        Node* simple_rotation_right() {
            std::cout << "\tsimple left rotation at node " << data << '\n';
            auto k1 = right;
            if (k1) {
                std::cout << "\t\t(switching " << k1->right->data << " with " << left->data << ")\n";
                right = k1->left;
                k1->left = this;
            } else {
                std::cout << "\t\t(nothing happening feijoada)\n";
            }

            height_ = std::max(height(left), height(right)) + 1;
            if (k1) {
                k1->height_ = std::max(height(k1->left), height(this)) + 1;
            }

            return k1;
        }

        Node* double_rotation_left() {
            std::cout << "\tdouble left rotation at node " << data << '\n';
            if (left) {
                left = left->simple_rotation_right();
            }
            return simple_rotation_left();
        }

        Node* double_rotation_right() {
            std::cout << "\tdouble right rotation at node " << data << '\n';
            if (right) {
                right = right->simple_rotation_left();
            }
            return simple_rotation_right();
        }
    };

    Node* root{nullptr};
    std::size_t size_{0u};
};

}

#endif
