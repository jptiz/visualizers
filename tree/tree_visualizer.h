#ifndef VISUALIZER_TREE_H
#define VISUALIZER_TREE_H

#include <array>
#include <iostream>
#include <tuple>
#include <utility>

#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>

#include "tree_shitter.h"

namespace structures::visualizer {

template <typename Tree, typename T>
class TreeVisualizer {
public:
    TreeVisualizer(const std::vector<T>& values):
        values_{values}
    {
        if (not font.loadFromFile(font_family)) {
            std::cout << "failed to load " << font_family << std::endl;
        }

        help.setFont(font);
        help.setCharacterSize(12);
        help.setString(
                L"N: Insere próximo elemento\n"
                 "P: Desfaz última inserção\n"
                 "R: Reinicia visualização\n"
                 "Q: Fechar visualizador");
        help.setFillColor(sf::Color{0, 0, 0});

        status.setFont(font);
        status.setCharacterSize(12);
        status.setString(L"Status: -");
        status.setFillColor(sf::Color{0, 0, 0});
        status.setPosition({0.f, help.getLocalBounds().height + 8});
    }

    auto values() const {
        return values_;
    }

    void set_values(const std::vector<T>& values) {
        values_ = values;
    }

    void set_font(sf::Font font) {
        font = std::move(font);
    }

    void start() {
        std::cout << "Starting visualization.\n";
        print_tree_info(tree);

        auto settings{sf::ContextSettings{}};
        settings.antialiasingLevel = 8;

        sf::RenderWindow window(sf::VideoMode{800, 600}, "Tree Visualizer", sf::Style::Default, settings);
        window.setFramerateLimit(60);

        while (window.isOpen()) {
            sf::Event event;
            while (window.pollEvent(event)) {

                switch (event.type) {
                    case sf::Event::Closed:
                        window.close();
                        break;
                    case sf::Event::KeyReleased:
                        switch (event.key.code) {
                            case sf::Keyboard::N:
                                next();
                                break;
                            case sf::Keyboard::P:
                                previous();
                                break;
                            case sf::Keyboard::Q:
                                window.close();
                                break;
                            case sf::Keyboard::R:
                                reset();
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            window.clear(sf::Color{255, 255, 255});

            for (auto& node: nodes) {
                if (node.line_left[1].position.x > 0) {
                    window.draw(node.line_left.data(), 2, sf::Lines);
                }
                if (node.line_right[1].position.x > 0) {
                    window.draw(node.line_right.data(), 2, sf::Lines);
                }
            }

            for (auto& node: nodes) {
                window.draw(node.circle);
                window.draw(node.text);
            }
            window.draw(help);
            window.draw(status);

            window.display();
        }
    }

    void previous() {
        if (current_value > 0u) {
            --current_value;
            status.setString(
                    std::wstring(L"Status: Removido o nodo ") +
                    std::to_wstring(values_[current_value]));
        } else {
            status.setString(L"Status: A árvore já está vazia.");
        }
        update();
    }

    void next() {
        if (current_value < values_.size()) {
            status.setString(
                    std::wstring(L"Status: Inserido o nodo ") +
                    std::to_wstring(values_[current_value]));
            ++current_value;
        } else {
            status.setString(L"Status: Sem mais elementos para inserir.");
        }
        update();
    }

    void update() {
        if (current_value > values_.size()) {
            return;
        }

        tree.clear();
        for (auto i = 0u; i < current_value; ++i) {
            tree.insert(values_[i]);
        }
        regen_nodes();
    }

    void reset() {
        nodes.clear();
        current_value = 0u;
        values_.clear();
        status.setString(L"Visualização reiniciada.");
    }

private:
    struct VisualNode {
        sf::CircleShape circle;
        sf::Text text;
        std::array<sf::Vertex, 2> line_left;
        std::array<sf::Vertex, 2> line_right;
    };

    void regen_nodes() {
        nodes.clear();
        create_nodes(root(tree), 400, 800u, 0);
    }

    sf::Vector2f create_nodes(
            const Node<T>* node,
            int x, std::size_t width, std::size_t height) {
        if (not node) {
            return sf::Vector2f{-300, -300};
        }

        width >>= 2;
        auto left = create_nodes(node->left, x - width, width, height + 1);
        auto right = create_nodes(node->right, x + width, width, height + 1);

        const auto radius = 30.f;

        auto x_{x - radius};
        auto y_{80.f + height * (radius * 2 + 20)};

        sf::Vertex line_left[2] = {
            sf::Vertex{sf::Vector2f{x_ + radius, y_ + radius}},
            sf::Vertex{left + sf::Vector2f{radius, radius}},
        };

        sf::Vertex line_right[2] = {
            sf::Vertex{sf::Vector2f{x_, y_} + sf::Vector2f{radius, radius}},
            sf::Vertex{right + sf::Vector2f{radius, radius}},
        };

        line_left[0].color =
            line_left[1].color =
            line_right[0].color =
            line_right[1].color = sf::Color{0, 0, 0};

        auto circle{sf::CircleShape{radius}};
        circle.setFillColor(sf::Color{0, 192, 0});
        circle.setOutlineColor(sf::Color{0, 64, 0});
        circle.setOutlineThickness(2.f);
        circle.setPosition(x - radius, 80.f + height * 80);

        auto text{sf::Text{}};
        text.setFillColor(sf::Color{0, 0, 0});
        text.setString(std::to_string(node->data));
        text.setFont(font);
        text.setPosition(
                x - text.getLocalBounds().width / 2 - 1,
                90.f + height * 80
        );

        nodes.push_back(VisualNode{circle, text, {line_left[0], line_left[1]}, {line_right[0], line_right[1]}});

        return sf::Vector2f{x_, y_};
    }

    Tree tree;
    std::vector<T> values_;
    std::size_t current_value{0u};
    std::vector<VisualNode> nodes{};

    sf::Font font;
    sf::Text help;
    sf::Text status;
    std::string font_family{"calibri.ttf"};
};

template <typename T, typename U>
void visualize_tree(const std::vector<U>& values) {
    auto visualizer{TreeVisualizer<T, U>{values}};
    visualizer.start();
}

}

#endif
