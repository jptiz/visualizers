#ifndef STRUCTURES_TREE_SHITTER_H
#define STRUCTURES_TREE_SHITTER_H

namespace structures {

template <typename T>
struct Node {
    T data;
    std::size_t height{0u};
    Node* left{nullptr};
    Node* right{nullptr};
};

template <typename T, typename U>
const auto offset(const T* base, std::size_t bytes) {
    return reinterpret_cast<const U*>(reinterpret_cast<const char*>(base) + bytes);
}

template <typename T>
const auto& root(const AVLTree<T>& tree) {
    return *offset<const AVLTree<T>, Node<T>*>(&tree, 0);
}

template <typename T>
const auto& size(const AVLTree<T>& tree) {
    return *offset<const AVLTree<T>, std::size_t>(&tree, sizeof(Node<T>*));
}

template <typename T>
void print_tree_info(const AVLTree<T>& tree) {
    std::cout
        << "Tree:\n"
        << "  root address: " << root(tree) << '\n'
        << "  size:         " << size(tree) << '\n';
}

template <typename T>
void print_node_info(const Node<T>* node) {
    std::cout
        << "Node (" << node->data << "): \n"
        << "  height: " << node->height << '\n'
        << "  left: " << node->left << '\n'
        << "  right: " << node->right << '\n';
}

}

#endif
