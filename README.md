Visualizadores de Estruturas de Dados
=====================================

Sobre
-----

Os visualizadores deste repositório têm como objetivo auxiliar os estudantes da
disciplina de Estruturas de Dados na UFSC (Universidade Federal de Santa
Catarina).

A proposta é que os visualizadores peguem a implementação das estruturas,
feitas pelos alunos, e as mostre visualmente, a fim de detectar melhor:

1. Quais inserções/remoções causam erro;
2. Qual o estado das estruturas em cada momento.


Requisitos
----------

É necessário ter instalado:

1. A biblioteca [SFML](https://www.sfml-dev.org), versão **2.4 ou superior**.
   No site se encontram instruções de como instalar a biblioteca em cada SO e
   plataforma de desenvolvimento (VisualStudio, por exemplo).

2. Para usuários windows: é necessário ter o `make` instalado caso queira
   aproveitar o arquivo pronto para compilação. O Eclipse provavelmente instala
   junto, mas, se não o fizer, instale `msys2`. Na dúvida, tente executar os
   comandos na parte de "Como utilizar".


Como utilizar
-------------

Para utilizar:

1. Clone este repositório (ou baixe-o manualmente no link de download à
   direita-acima da lista de arquivos do repositório):

    ```bash
    git clone https://gitlab.com/jptiz/visualizers
    ```

2. Vá na pasta da estrutura que deseja visualizar (no caso das árvores, a pasta
   "tree" serve para todas).

3. Altere o arquivo `makefile`, adicionando a pasta com o header da sua
   implementação:

    ```make
    # Configs. gerais
    OUTPUT     := tree_visualizer
    STRUCTPATH := pasta/do/seu/header/
    STRUCT     := ClasseDaSuaEstrutura

    # (... resto do makefile ...)
    ```

    Por exemplo: Se você quer visualizar a sua árvore AVL (cuja classe é
    `AVLTree`), e o caminho do header dela está em
    `C:/Users/jptiz/Documents/avl_tree.h`, basta editar o `makefile` da forma:

    ```make
    # Configs. gerais
    OUTPUT     := tree_visualizer
    STRUCTPATH := C:/Users/jptiz/Documents/avl_tree.h
    STRUCT     := AVLTree
    ```

    OBS: Caminhos que comecem com `/` ou `C:/` são absolutos, senão são relativos à
    pasta atual.

4. Chame o comando `make` (na pasta do visualizador).

5. Execute o visualizador, mandando os valores a serem inseridos como parâmetro:

    Explorador de arquivos: clique duplo no arquivo (ou selecione-o e pressione
    Enter);

    Terminal (inserindo os valores 1, 2, -5, -15, 3, 0, 2):

    ```bash
    ./tree_visualizer 1 2 -5 -15 3 0 2
    ```

    CMD (Windows):

    ```bash
    tree_visualizer.exe 1 2 -5 -15 3 0 2
    ```

    Se os valores não forem inseridos, por padrão os valores serão de 1 a 15.

6. Caso edite seu código e o `make` diga "nothing to be done for 'all'",
   execute o comando `make clean`.
